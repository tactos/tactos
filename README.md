# Tactos

## Warning
This software has been developed during research project from  april 2019 to may 2021. The project is quite usable but should still require tests and improvement to be easy to use.

## Content

This repository aim to regroup and describe all part of Tactos project. Indeed, Tactos is composed by several part more or less usable.

## Description of the project

This project aims te create a Assistive Technologie system to help blind peoples to use digital technologies on GNU/Linux.
Constitued by a computer and a device with 2 braille cells, the project gives 
tactile feedback relatively to the content of the screen around the mouse pointer. Because of the user awareness of his own gesture with
 the mouse, he can recognize shapes on the screen. 

![Tactos example](tactos_all.svg)

To achieve this goal, Tactos is composed by a set of software organized as follow :

![Tactos architecture](doc/img/architecture.svg)

## Parts 

### [Tactos-Core-color](../../../../tactos-core-color)

Main part of the project, this software focus on the pixels colors on the screen. We configure somme colors or range of colors that will triggers the device pins if the mouse go hover.
Moreover, colors can be associated with semantic information that can be said by speech synthesis. 
Using this page as an example, we could associate the **blue** color with the information **"clone button"**. Then when the user put his mouse over the **blue "clone"** button, pins raises and TTS say  "clone button".

More precisely, this software trigger the following pipeline on each mouse movement :
- Capture a 32 pixels square of the screen around the mouse pointer.
- Split this square in a grid of 16 cells (corresponding to the 2 brailles cells)
- Check in each cell for pixels with specified color
- Send through DBus interface the boolean grid representation
- Select the speech to be said and say it.

### [Tactos-dbus](../../../../tactos-dbus)

InterProcess communication manager to send matrix value from tactos-core to any client. It expose a dbus interface on `org/tactos/ipc`.

### [Tactos-MIT4](../../../../tactos-mit4)

Tactos-MIT4 is a driver to manage braille device created by [University of Technology of Compiegne (UTC)](https://www.utc.fr/). It as a GTK Gui to perform bluetooth connection with the device.

### [Tactos-mit5](../../../../tactos-mit5)

Driver for the new version on MIT. This version uses usb connection and enable button usage.

### [Tactos-Term](../../../../tactos-term)

Debug ncurses viewer for the matrix. Act like a client and display the matrix in real time.

### [Tactos-Pad](../../../../tactos-pad) [WIP]

A basic python program to set touchpad in absolute mode. That means that putting
your finger on top-left of the touchpad will cause the pointer to move to 
top-left part of the screen. This part has not been heavily tested due to lack of precision when mapping the whole screen on the small touchpad. Using a tablet or having some zoom option seems needed.

### [Tactos-Core-AT-SPI](../../../../tactos-core-at-spi) [WIP]

Python program that use Accessibility (AT-SPI) API to send feedbacks to braille
device. It detect Accessible element border and say the name of the element through
speech synthesis.

This part seems very interesting because we can skip the step of configuring each color with information. However, this is only usable on software interface context and necessite already well accessible software.

### Dépendencies

- libopencv-core3.2
- libyaml-cpp0.6
- python3-pydbus
- libxtst6
- libgtkmm-3.0-1v5
- libxaw7
- speech-dispatcher
- bluetooth
- python3-psutil
- python3-pydbus
- python3-numpy

## building
###  dev dependencies

- g++ 
- meson 
- libgtkmm-3.0-dev
- libopencv-dev 
- libxtst-dev 
- libxaw7-dev 
- libyaml-cpp-dev 
- libspeechd-dev
- libbluetooth-dev
- libncurses-dev
- python3-psutil
- python3-pydbus
- python3-numpy
- libserial-dev (optional can be built as dependency)
- libxml2-utils (optonal to reduce xml file size)


### Compilation

```bash
meson builddir # use --buildtype=release  if you don't want debug option
cd builddir
ninja
```

### installation

```
cd builddir
sudo ninja install
```
### Run

Run `tactos-core-color` for desktop detection and `mit5`, `tactos-mit4` or `tactos-term` to get feedbacks.
 
 You may need to to run the command `systemctl daemon-reload` if the tactos-dbus doesn't start properly

## Contact

This project is developed by the the laboratory of [Costech](http://www.costech.utc.fr/) in the [University of Technology of Compiegne (UTC)](https://www.utc.fr/) in partnership with [hypra](http://hypra.fr/-Accueil-1-.html) and [natbraille](http://natbraille.free.fr/).
